package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// "github.com/gin-gonic/gin"

var (
	// Version set by ldflags
	Version string
	// GitCommit set by ldflags
	GitCommit string
)

func main() {
	r := gin.Default()
	r.GET("/ping", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"message": "PONG"})
	})
	// curl  localhost:8080/hello/mario
	r.GET("/hello/:name", hello)
	// curl  localhost:8080/welcome/?name=Mario
	r.GET("/welcome/", welcome)
	r.Run()
}

func hello(c *gin.Context) {
	name := c.Param("name")
	c.String(http.StatusOK, "Hello %s", name)
}

func welcome(c *gin.Context) {
	name := c.DefaultQuery("name", "noname")
	c.String(http.StatusOK, "Welcome %s", name)
}
