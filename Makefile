
help:
	@echo "install"
	@echo "dep"
	@echo "emb  (embedmd)"
	@echo "build"
	@echo "clean"


install: deps
	govendor sync


deps:
	@hash govendor > /dev/null 2>&1; if [ $$? -ne 0 ]; then \
		go get -u github.com/kardianos/govendor; \
	fi
	@hash embedmd > /dev/null 2>&1; if [ $$? -ne 0 ]; then \
		go get -u github.com/campoy/embedmd; \
	fi


.PHONY: emb
emb:
	embedmd -w *.md


.PHONY: build
build:
	go build -ldflags="-X main.Version=v0.0.1 -X main.GitCommit=$(shell git log --pretty=format:'%h' -n 1)"


.PHONY: clean
clean:
	go clean